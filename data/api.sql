-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- PostgreSQL version: 9.3
-- Project Site: pgmodeler.com.br
-- Model Author: Anton Redozubov (anton@redozubov.ru)

SET check_function_bodies = false;
-- ddl-end --

-- -- object: mif | type: ROLE --
-- CREATE ROLE mif WITH 
-- 	UNENCRYPTED PASSWORD 'mif';
-- -- ddl-end --
-- 

-- Database creation must be done outside an multicommand file.
-- These commands were put in this file only for convenience.
-- -- object: mif | type: DATABASE --
-- -- Main DB --
-- CREATE DATABASE mif
-- 	ENCODING = 'UTF-8'
-- 	LC_COLLATE = 'ru_RU.UTF-8'
-- 	LC_CTYPE = 'ru_RU.UTF-8'
-- 	TABLESPACE = pg_default
-- 	OWNER = mif
-- ;
-- -- ddl-end --
--

DROP SCHEMA api CASCADE;

-- object: api | type: SCHEMA --
CREATE SCHEMA api;
ALTER SCHEMA api OWNER TO mif;
COMMENT ON SCHEMA api IS 'API scheme';
-- ddl-end --
-- ddl-end --

SET search_path TO pg_catalog,public,api;
-- ddl-end --

-- object: api.cinema | type: TABLE --
CREATE TABLE api.cinema(
	idcinema char(32) NOT NULL,
	name text,
	CONSTRAINT cinema_pk PRIMARY KEY (idcinema)

);
-- ddl-end --
-- object: cinema_name_idx | type: INDEX --
CREATE INDEX cinema_name_idx ON api.cinema
	USING btree
	(
	  name ASC NULLS LAST
	);
-- ddl-end --


COMMENT ON COLUMN api.cinema.idcinema IS 'Hash';
-- ddl-end --
COMMENT ON COLUMN api.cinema.name IS 'Cinema name';
-- ddl-end --
ALTER TABLE api.cinema OWNER TO mif;
-- ddl-end --

-- object: api.hall | type: TABLE --
CREATE TABLE api.hall(
	idhall serial NOT NULL,
	idcinema char(32) NOT NULL,
	name text,
	seats int4 NOT NULL,
	CONSTRAINT hall_pk PRIMARY KEY (idhall)

);
-- ddl-end --
COMMENT ON TABLE api.hall IS 'Cinema hall';
-- ddl-end --
COMMENT ON COLUMN api.hall.idcinema IS 'LInk to cinema';
-- ddl-end --
COMMENT ON COLUMN api.hall.seats IS 'Max seats';
-- ddl-end --
ALTER TABLE api.hall OWNER TO mif;
-- ddl-end --

-- object: api.film | type: TABLE --
CREATE TABLE api.film(
	idfilm char(32) NOT NULL,
	name text NOT NULL,
	CONSTRAINT film_pk PRIMARY KEY (idfilm)

);
-- ddl-end --
-- object: film_name_idx | type: INDEX --
CREATE INDEX film_name_idx ON api.film
	USING btree
	(
	  name ASC NULLS LAST
	);
-- ddl-end --


COMMENT ON COLUMN api.film.idfilm IS 'Md5 from name';
-- ddl-end --
COMMENT ON COLUMN api.film.name IS 'Film name';
-- ddl-end --
ALTER TABLE api.film OWNER TO mif;
-- ddl-end --

-- object: api.session | type: TABLE --
CREATE TABLE api.session(
	idsession serial NOT NULL,
	idfilm char(32) NOT NULL,
	idhall int4 NOT NULL,
	sessionts timestamp NOT NULL,
	CONSTRAINT session_pk PRIMARY KEY (idsession)

);
-- ddl-end --
COMMENT ON TABLE api.session IS 'Film session schelule';
-- ddl-end --
COMMENT ON COLUMN api.session.idfilm IS 'Link to film';
-- ddl-end --
COMMENT ON COLUMN api.session.idhall IS 'Link to cinema hall';
-- ddl-end --
COMMENT ON COLUMN api.session.sessionts IS 'Film show timestamp';
-- ddl-end --
ALTER TABLE api.session OWNER TO mif;
-- ddl-end --

-- object: api.order | type: TABLE --
CREATE TABLE api.order(
	idorder char(32) NOT NULL,
	isactive bool NOT NULL DEFAULT true,
	idsession int4 NOT NULL,
	CONSTRAINT order_pk PRIMARY KEY (idorder)

);
-- ddl-end --
-- object: order_isactive_idx | type: INDEX --
CREATE INDEX order_isactive_idx ON api.order
	USING btree
	(
	  isactive ASC NULLS LAST
	);
-- ddl-end --


COMMENT ON TABLE api.order IS 'Tickets orders';
-- ddl-end --
COMMENT ON COLUMN api.order.idorder IS 'Hash';
-- ddl-end --
COMMENT ON COLUMN api.order.idsession IS 'Link to schedule';
-- ddl-end --
ALTER TABLE api.order OWNER TO mif;
-- ddl-end --

-- object: api.ticket | type: TABLE --
CREATE TABLE api.ticket(
	idticket serial NOT NULL,
	idorder char(32) NOT NULL,
	seat int4 NOT NULL,
	CONSTRAINT ticket_pk PRIMARY KEY (idticket)

);
-- ddl-end --
COMMENT ON TABLE api.ticket IS 'Ticket in order';
-- ddl-end --
COMMENT ON COLUMN api.ticket.idorder IS 'Link to order';
-- ddl-end --
ALTER TABLE api.ticket OWNER TO mif;
-- ddl-end --

-- object: hall_cinema_idcinema_fk | type: CONSTRAINT --
ALTER TABLE api.hall ADD CONSTRAINT hall_cinema_idcinema_fk FOREIGN KEY (idcinema)
REFERENCES api.cinema (idcinema) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;
-- ddl-end --


-- object: session_film_fk | type: CONSTRAINT --
ALTER TABLE api.session ADD CONSTRAINT session_film_fk FOREIGN KEY (idfilm)
REFERENCES api.film (idfilm) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;
-- ddl-end --


-- object: session_hall_fk | type: CONSTRAINT --
ALTER TABLE api.session ADD CONSTRAINT session_hall_fk FOREIGN KEY (idhall)
REFERENCES api.hall (idhall) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;
-- ddl-end --


-- object: order_session_fk | type: CONSTRAINT --
ALTER TABLE api.order ADD CONSTRAINT order_session_fk FOREIGN KEY (idsession)
REFERENCES api.session (idsession) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;
-- ddl-end --


-- object: ticket_order_fk | type: CONSTRAINT --
ALTER TABLE api.ticket ADD CONSTRAINT ticket_order_fk FOREIGN KEY (idorder)
REFERENCES api.order (idorder) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION NOT DEFERRABLE;
-- ddl-end --



