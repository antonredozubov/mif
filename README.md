# Тестовое задание в МИФ

Давно и с удовольствием читаю различные книги вашего издательства.

Очень интересная у вас модель работы. Давно знаком с ней. И очень надеюсь, что смогу взглянуть и поработать с вами внутри.

## REST-подобное API для покупки билетов в кино

### Задача

Существует несколько кинотеатров, в каждом которых по несколько залов. В них идёт несколько фильмов, некоторые фильмы в нескольких залах разных кинотеатров одновременно. У фильмов есть сеансы, основным свойством которых является время сеанса. Так же в каждом зале определённое количество мест.

Нужно пользователю дать возможность просмотреть расписание кинотеатра, с возможностью фильтрации по залу:

    :::http
    GET /api/cinema/<название кинотеатра>/schedule[?hall=номер зала]

Также надо дать возможность просмотреть в каких кинотеатрах/залах идёт конкретный фильм:


    :::http
    GET /api/film/<название фильма>/schedule

Затем надо проверить, какие места свободны на конкретный сеанс:


    :::http
    GET /api/session/<id сеанса>/places

И дать возможность купить билет:


    :::http
    POST /api/tickets/buy?session=<id сеанса>&places=1,3,5,7

Результатом запроса должен быть уникальный код, характеризующий этот набор билетов

И отменить покупку, но не раньше, чем за час до начала сеанса:

    :::http
    POST /api/tickets/reject/<уникальный код>

Улучшения, позволяющие комфортнее взаимодействовать с сервисом, оставляются на ваше усмотрение. Как и формат url и набор передаваемых параметров.

# Module

# Install

* install module
    * composer.json
    * git submodule
* add urlManager rules (mif/config/rules.php) to config
* add module to config
* fill db "yiic migrate --migrationPath=application.modules.mif.migrations"

# Usage

## Error

### Response

#### Headers

    :::http
    HTTP/1.1 409 Conflict
    Server: nginx
    Date: Tue, 20 May 2014 12:33:55 GMT
    Content-Type: application/json
    Transfer-Encoding: chunked
    Connection: keep-alive

#### JSON

    :::json
    {
        "status":409,
        "message":"Order already rejected"
    }

## Help

### Request

    :::bash
    curl -i http://mif.local/api/

### Response

#### Headers

    :::http
    HTTP/1.1 200 OK
    Server: nginx
    Date: Tue, 20 May 2014 11:58:57 GMT
    Content-Type: application/json
    Transfer-Encoding: chunked
    Connection: keep-alive
    Vary: Accept-Encoding

#### JSON

    :::json
    {
        "requests":{
            "GET /api/help":"This help",
            "GET /api/cinema":"Get all cinemas",
            "GET /api/cinema/<idcinema:\w+>":"Get cinema halls",
            "GET /api/cinema/<idcinema:\w+>/schedule[?hall=<hall:\w+>]":"Get cinema/hall schedule",
            "GET /api/film":"Get all films",
            "GET /api/film/<idfilm:\w+>/schedule":"Get film schedule",
            "GET /api/session/<idsession:\w+>/seats":"Get available seats",
            "POST /api/ticket/buy?session=<session:\w+>&seats=<seats:[0-9,]+>":"Buy tickets with seats",
            "POST /api/ticket/reject/<idorder:\w+>":"Cancel order"
        }
    }

#### Errors

None

## Cinema list

### Request

    :::bash
    curl -i http://mif.local/api/cinema/

### Response

#### Headers

    :::http
    HTTP/1.1 200 OK
    Server: nginx
    Date: Tue, 20 May 2014 12:10:41 GMT
    Content-Type: application/json
    Transfer-Encoding: chunked
    Connection: keep-alive
    Vary: Accept-Encoding

#### JSON

    :::json
    [
        {
            "idcinema":"b69683442085192fedf30f445edce3dd",
            "name":"Пионер"
        },
        {
            "idcinema":"199b5d4eb692124aeb3f17b164973e37",
            "name":"Иллюзион"
        },
        {
            "idcinema":"a796294e2ece16189d768a47a2d05416",
            "name":"Балтика"
        }
    ]

#### Errors

- 404 No cinema

## Cinema hall list

### Request

    :::bash
    curl -i http://mif.local/api/cinema/b69683442085192fedf30f445edce3dd/

### Response

#### Headers

    :::http
    HTTP/1.1 200 OK
    Server: nginx
    Date: Tue, 20 May 2014 12:12:42 GMT
    Content-Type: application/json
    Transfer-Encoding: chunked
    Connection: keep-alive
    Vary: Accept-Encoding

#### JSON

    :::json
    [
        {
            "idhall":1,
            "idcinema":"b69683442085192fedf30f445edce3dd",
            "name":"Большой",
            "seats":144
        },
        {
            "idhall":2,
            "idcinema":"b69683442085192fedf30f445edce3dd",
            "name":"Средний",
            "seats":88
        },
        {
            "idhall":3,
            "idcinema":"b69683442085192fedf30f445edce3dd",
            "name":"Малый",
            "seats":175
        }
    ]

#### Errors

- 404 No such cinema
- 404 No hall

## Cinema schedule

### Request

    :::bash
    curl -i http://mif.local/api/cinema/b69683442085192fedf30f445edce3dd/schedule/

With hall select

    :::bash
    curl -i http://mif.local/api/cinema/b69683442085192fedf30f445edce3dd/schedule/?hall=1


### Response

#### Headers

    :::http
    HTTP/1.1 200 OK
    Server: nginx
    Date: Tue, 20 May 2014 12:37:53 GMT
    Content-Type: application/json
    Transfer-Encoding: chunked
    Connection: keep-alive
    Vary: Accept-Encoding

#### JSON

    :::json
    [
    	{
    		"session_id":74,
    		"session_date":"2014-05-21 10:04:39",
    		"cinema_name":"Пионер",
    		"hall_name":"Большой",
    		"film_name":"1+1"
    	},
    	{
    		"session_id":44,
    		"session_date":"2014-05-22 19:20:16",
    		"cinema_name":"Пионер",
    		"hall_name":"Большой",
    		"film_name":"Годзилла"
    	}
    ]

#### Errors

- 404 No such cinema
- 400 No such hall in cinema
- 404 No sessions

## Film list

### Request

    :::bash
    curl -i http://mif.local/api/film/

### Response

#### Headers

    :::http
    HTTP/1.1 200 OK
    Server: nginx
    Date: Tue, 20 May 2014 12:43:16 GMT
    Content-Type: application/json
    Transfer-Encoding: chunked
    Connection: keep-alive
    Vary: Accept-Encoding

#### JSON

    :::json
    [
    	{
    		"idfilm":"bcf3169e454ba1276d7e4753044dde56",
    		"name":"Люди Икс: Дни минувшего будущего"
    	},
    	{
    		"idfilm":"1d881b7bb8d146768c89a7deb4a1078c",
    		"name":"Малефисента"
    	},
    	{
    		"idfilm":"8f91e85d5ed8494dca8d6ebaf73bbb9e",
    		"name":"Годзилла"
    	}
    ]

#### Errors

- 404 No film

## Film shedule

### Request

    :::bash
    curl -i http://mif.local/api/film/5a2c87412dd81d8e607200030b000d85/schedule/

### Response

#### Headers

    :::http
    HTTP/1.1 200 OK
    Server: nginx
    Date: Tue, 20 May 2014 12:45:32 GMT
    Content-Type: application/json
    Transfer-Encoding: chunked
    Connection: keep-alive
    Vary: Accept-Encoding

#### JSON

    :::json
    [
    	{
    		"session_id":138,
    		"session_date":"2014-05-22 13:23:08",
    		"cinema_name":"Синема Стар",
    		"hall_name":"Основной",
    		"film_name":"Королевство полной луны"
    	},
    	{
    		"session_id":144,
    		"session_date":"2014-05-25 18:31:54",
    		"cinema_name":"Иллюзион",
    		"hall_name":"Малый",
    		"film_name":"Королевство полной луны"
    	}
    ]

#### Errors

- 404 No such film
- 404 No session

## Session seats

### Request

    :::bash
    curl -i http://mif.local/api/session/136/seats/

### Response

#### Headers

    :::http
    HTTP/1.1 200 OK
    Server: nginx
    Date: Tue, 20 May 2014 13:16:17 GMT
    Content-Type: application/json
    Transfer-Encoding: chunked
    Connection: keep-alive
    Vary: Accept-Encoding

#### JSON

    :::json
    {
    	"session_id":136,
    	"session_date":"2014-06-29 23:38:59",
    	"cinema_name":"Формула Кино",
    	"hall_name":"Малый",
    	"total_seats":216,
    	"film_name":"Королевство полной луны ",
    	"occupied_seats":[
    		1,
    		2
    	]
    }

#### Errors

- 404 No session

## Buy ticket

### Request

    :::bash
    curl -i --data "session=136&seats=3,4,5" http://mif.local/api/ticket/buy/

### Response

#### Headers

    :::http
    HTTP/1.1 200 OK
    Server: nginx
    Date: Tue, 20 May 2014 13:35:59 GMT
    Content-Type: application/json
    Transfer-Encoding: chunked
    Connection: keep-alive
    Vary: Accept-Encoding

#### JSON

    :::json
    {
        "idorder":"dc6d67f9565e6c3d5d732e2ade4a71cd",
        "isactive":true
    }

#### Errors

- 400 No session or seats
- 404 No such session
- 400 Wrong seat
- 409 Some seats are occupied
- 500 Order creation error
- 500 Ticket creation error

## Reject ticket

### Request

    :::bash
    curl -i --data "" http://mif.local/api/ticket/reject/dc6d67f9565e6c3d5d732e2ade4a71cd/

### Response

#### Headers

    :::http
    HTTP/1.1 200 OK
    Server: nginx
    Date: Tue, 20 May 2014 13:44:29 GMT
    Content-Type: application/json
    Transfer-Encoding: chunked
    Connection: keep-alive
    Vary: Accept-Encoding

#### JSON

    :::json
    {
        "idorder":"dc6d67f9565e6c3d5d732e2ade4a71cd",
        "isactive":false
    }

#### Errors

- 404 No such order
- 410 Too late to reject
- 409 Order already rejected
- 500 Order reject error

## ToDo

- Add logging
- Add backbone interface
- Test composer
- ToDo from code