<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * UrlManager rules for module
 *
 * @category  Config
 * @package   MIF
 * @author    Anton Redozubov <anton@redozubov.ru>
 * @copyright 2014 Anton Redozubov
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://redozubov.ru
 */

return [
    // REST API patterns
    ['mif/help', 'pattern' => 'api', 'verb' => 'GET'],
    ['mif/help', 'pattern' => 'api/help', 'verb' => 'GET'],

    ['mif/cinema/getList', 'pattern' => 'api/cinema', 'verb' => 'GET'],
    ['mif/cinema/getHall', 'pattern' => 'api/cinema/<idcinema:\w+>', 'verb' => 'GET'],
    ['mif/cinema/getSchedule', 'pattern' => 'api/cinema/<idcinema:\w+>/schedule', 'verb' => 'GET'],

    ['mif/film/getList', 'pattern' => 'api/film', 'verb' => 'GET'],
    ['mif/film/getSchedule', 'pattern' => 'api/film/<idfilm:\w+>/schedule', 'verb' => 'GET'],

    ['mif/session/getSeats', 'pattern' => 'api/session/<idsession:\w+>/seats', 'verb' => 'GET'],

    ['mif/ticket/buy', 'pattern' => 'api/ticket/buy', 'verb' => 'POST'],
    ['mif/ticket/reject', 'pattern' => 'api/ticket/reject/<idorder:\w+>', 'verb' => 'POST'],
];