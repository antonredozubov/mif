<?php

class m140520_131208_base_db extends CDbMigration
{
    public function up()
    {

        $base_db = file_get_contents(realpath(__DIR__) . '/../data/api.sql');

        $this->execute($base_db);

        $cinemas = [
            'Пионер'         => ['Большой', 'Средний', 'Малый'],
            'Иллюзион'       => ['Большой 1', 'Большой 2', 'Средний', 'Малый'],
            'Балтика'        => ['Большой', 'Малый'],
            'Формула Кино'   => ['Средний 1', 'Средний 2', 'Малый'],
            'Синема Стар'    => ['Основной', 'Малый'],
            'Октябрь'        => ['Основной', 'Малый'],
            'Художественный' => ['Основной'],
            'Молодежный'     => ['Главный'],
            '35ММ'           => ['Основной'],
            'Музеон'         => ['Открытый'],
        ];

        $halls = [];

        foreach ($cinemas as $cinema_name => $cinema_halls)
        {
            $this->insert('api.cinema', [
                                        'idcinema' => md5($cinema_name),
                                        'name'     => $cinema_name,
                                        ]);
            foreach ($cinema_halls as $cinema_hall)
            {
                $this->insert('api.hall', [
                                          'idcinema' => md5($cinema_name),
                                          'name'     => $cinema_hall,
                                          'seats'    => rand(50, 250),
                                          ]);
                $halls[] = Yii::app()->db->getCommandBuilder()->getLastInsertID('api.hall');
            }
        }

        $films = [
            'Люди Икс: Дни минувшего будущего',
            'Малефисента',
            'Годзилла',
            'Венера в мехах',
            '1+1',
            'Гадкий я',
            'Мадагаскар',
            'Хранители снов',
            'Королевство полной луны',
            'Начало',
        ];

        foreach ($films as $film)
        {
            $this->insert('api.film', [
                                      'idfilm' => md5($film),
                                      'name'   => $film,
                                      ]);

            for ($session = 0, $total_session = sizeof($halls) * 3; $session < $total_session; $session++)
            {
                if (2 == rand(1, 3))
                {
                    $this->insert('api.session', [
                                                 'idfilm'    => md5($film),
                                                 'idhall'    => $halls[rand(0, sizeof($halls) - 1)],
                                                 'sessionts' => date('Y-m-d G:i:s', mktime(date('H') + rand(10, 1000), date('i') + rand(10, 1000), date('s') + rand(10, 1000), date('m'), date('d'), date('Y'))),
                                                 ]);
                }
            }
        }
    }

    public function down()
    {
        return true;
    }
}
