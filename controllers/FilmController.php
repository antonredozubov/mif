<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Film controller
 *
 * @category  Controller
 * @package   MIF
 * @author    Anton Redozubov <anton@redozubov.ru>
 * @copyright 2014 Anton Redozubov
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://redozubov.ru
 */

class FilmController extends Controller
{
    public function accessRules()
    {
        return array_merge(
            [['allow', 'actions' => ['getList', 'getSchedule'], 'users' => ['?']]],
            parent::accessRules()
        );
    }

    public function actionGetList()
    {
        if (null === ($model = Film::model()->findAll()))
        {
            throw new CHttpException(404, 'No film');
        }
        $this->sendResponse(200, CJSON::encode($model));
    }

    public function actionGetSchedule($idfilm)
    {
        if (MD5::isValidMd5($idfilm))
        {
            $film = Film::model()->findByPk($idfilm);
        }
        else
        {
            $film = Film::model()->findByAttributes(['name' => $idfilm]);
        }

        if (empty($film) or (is_array($film) and 0 == sizeof($film)))
        {
            throw new CHttpException(404, 'No such film');
        }

        $sessions = Yii::app()->db->createCommand()
                                  ->select('s.idsession as session_id, s.sessionts as session_date, c.name as cinema_name, h.name as hall_name, f.name as film_name')
                                  ->from('api.cinema as c')
                                  ->join('api.hall as h', 'c.idcinema = h.idcinema')
                                  ->join('api.session as s', 'h.idhall = s.idhall')
                                  ->join('api.film as f', 's.idfilm = f.idfilm')
                                  ->where('f.idfilm = :idfilm AND s.sessionts > now()', array(':idfilm' => $film->idfilm))
                                  ->order('s.sessionts')
                                  ->queryAll();

        if (empty($sessions) or (is_array($sessions) and 0 == sizeof($sessions)))
        {
            throw new CHttpException(404, 'No sessions');
        }
        $this->sendResponse(200, CJSON::encode($sessions));
    }
}
