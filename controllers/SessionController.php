<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Session controller
 *
 * @category  Controller
 * @package   MIF
 * @author    Anton Redozubov <anton@redozubov.ru>
 * @copyright 2014 Anton Redozubov
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://redozubov.ru
 */

class SessionController extends Controller
{
    public function accessRules()
    {
        return array_merge(
            [['allow', 'actions' => ['getSeats'], 'users' => ['?']]],
            parent::accessRules()
        );
    }

    public function actionGetSeats($idsession)
    {
        $session = Yii::app()->db->createCommand()
                                 ->select('s.idsession as session_id, s.sessionts as session_date, c.name as cinema_name, h.name as hall_name, h.seats as total_seats, f.name as film_name')
                                 ->from('api.cinema as c')
                                 ->join('api.hall as h', 'c.idcinema = h.idcinema')
                                 ->join('api.session as s', 'h.idhall = s.idhall')
                                 ->join('api.film as f', 's.idfilm = f.idfilm')
                                 ->where('s.idsession = :idsession AND s.sessionts > now()', array(':idsession' => $idsession))
                                 ->queryRow();

        if (empty($session))
        {
            throw new CHttpException(404, 'No session');
        }
        else
        {
            $seats = Yii::app()->db->createCommand()
                                   ->select('t.seat as occupied_seat')
                                   ->from('api.ticket as t')
                                   ->join('api.order as o', 'o.idorder = t.idorder and o.isactive is true')
                                   ->join('api.session as s', 's.idsession = o.idsession')
                                   ->where('s.idsession = :idsession', array(':idsession' => $idsession))
                                   ->queryColumn();

            if (is_array($seats) and 0 < sizeof($seats))
            {
                $session['occupied_seats'] = $seats;
            }
        }
        $this->sendResponse(200, CJSON::encode($session));
    }
}
