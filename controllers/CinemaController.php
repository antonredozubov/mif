<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Cinema controller
 *
 * @category  Controller
 * @package   MIF
 * @author    Anton Redozubov <anton@redozubov.ru>
 * @copyright 2014 Anton Redozubov
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://redozubov.ru
 */

class CinemaController extends Controller
{
    public function accessRules()
    {
        return array_merge(
            [['allow', 'actions' => ['getList', 'getHall', 'getSchedule'], 'users' => ['?']]],
            parent::accessRules()
        );
    }

    public function actionGetList()
    {
        if (null === ($model = Cinema::model()->findAll()))
        {
            throw new CHttpException(404, 'No cinema');
        }
        $this->sendResponse(200, CJSON::encode($model));
    }

    public function actionGetHall($idcinema)
    {
        if (MD5::isValidMd5($idcinema))
        {
            $cinema = Cinema::model()->findByPk($idcinema);
        }
        else
        {
            $cinema = Cinema::model()->findByAttributes(['name' => $idcinema]);
        }

        if (empty($cinema) or (is_array($cinema) and 0 == sizeof($cinema)))
        {
            throw new CHttpException(404, 'No such cinema');
        }
        if (null === ($hall = Hall::model()->findAllByAttributes(['idcinema' => $cinema->idcinema])))
        {
            throw new CHttpException(404, 'No hall');
        }
        $this->sendResponse(200, CJSON::encode($hall));
    }

    public function actionGetSchedule($idcinema)
    {
        if (MD5::isValidMd5($idcinema))
        {
            $cinema = Cinema::model()->findByPk($idcinema);
        }
        else
        {
            $cinema = Cinema::model()->findByAttributes(['name' => $idcinema]);
        }

        if (empty($cinema) or (is_array($cinema) and 0 == sizeof($cinema)))
        {
            throw new CHttpException(404, 'No such cinema');
        }

        $idhall = Yii::app()->request->getParam('hall');

        if (!is_null($idhall))
        {
            $hall = Yii::app()->db->createCommand()
                                  ->select('c.name as cinema_name, h.idhall as hall_id,')
                                  ->from('api.cinema as c')
                                  ->join('api.hall as h', 'c.idcinema = h.idcinema and h.idhall = :idhall', array(':idhall' => $idhall))
                                  ->where('c.idcinema = :idcinema', array(':idcinema' => $cinema->idcinema))
                                  ->queryRow();

            if (empty($hall) or (is_array($hall) and 0 == sizeof($hall)))
            {
                throw new CHttpException(400, 'No such hall in cinema');
            }
        }

        $sessions = Yii::app()->db->createCommand()
                                  ->select('s.idsession as session_id, s.sessionts as session_date, c.name as cinema_name, h.name as hall_name, f.name as film_name')
                                  ->from('api.cinema as c')
                                  ->join('api.hall as h', 'c.idcinema = h.idcinema and (:idhall is null or h.idhall = :idhall)', array(':idhall' => $idhall))
                                  ->join('api.session as s', 'h.idhall = s.idhall')
                                  ->join('api.film as f', 's.idfilm = f.idfilm')
                                  ->where('c.idcinema = :idcinema AND s.sessionts > now()', array(':idcinema' => $cinema->idcinema))
                                  ->order('s.sessionts')
                                  ->queryAll();

        if (empty($sessions) or (is_array($sessions) and 0 == sizeof($sessions)))
        {
            throw new CHttpException(404, 'No sessions');
        }
        $this->sendResponse(200, CJSON::encode($sessions));
    }
}
