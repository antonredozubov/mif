<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Output all available requests
 *
 * @category  Controller
 * @package   MIF
 * @author    Anton Redozubov <anton@redozubov.ru>
 * @copyright 2014 Anton Redozubov
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://redozubov.ru
 */

class HelpController extends Controller
{
    public function accessRules()
    {
        return array_merge(
            [['allow', 'actions' => ['index'], 'users' => ['?']]],
            parent::accessRules()
        );
    }

    public function actionIndex()
    {
        $help = [
            'requests' => [
                'GET /api/help'                                                    => 'This help',
                'GET /api/cinema'                                                  => 'Get all cinemas',
                'GET /api/cinema/<idcinema:\w+>'                                   => 'Get cinema halls',
                'GET /api/cinema/<idcinema:\w+>/schedule[?hall=<hall:\w+>]'        => 'Get cinema/hall schedule',
                'GET /api/film'                                                    => 'Get all films',
                'GET /api/film/<idfilm:\w+>/schedule'                              => 'Get film schedule',
                'GET /api/session/<idsession:\w+>/seats'                           => 'Get available seats',
                'POST /api/ticket/buy?session=<session:\w+>&seats=<seats:[0-9,]+>' => 'Buy tickets with seats',
                'POST /api/ticket/reject/<idorder:\w+>'                            => 'Cancel order',
            ]
        ];

        $this->sendResponse(200, CJSON::encode($help));
    }
}