<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Ticket controller
 *
 * @category  Controller
 * @package   MIF
 * @author    Anton Redozubov <anton@redozubov.ru>
 * @copyright 2014 Anton Redozubov
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://redozubov.ru
 */

class TicketController extends Controller
{
    public function accessRules()
    {
        // @todo: add authorisation
        return array_merge(
            [['allow', 'actions' => ['buy', 'reject'], 'users' => ['?']]],
            parent::accessRules()
        );
    }

    public function actionBuy()
    {
        // @todo: change to JSON
        $idsession = (int)Yii::app()->request->getParam('session');
        $seats     = Yii::app()->request->getParam('seats');

        if (is_null($idsession) or is_null($seats))
        {
            throw new CHttpException(400, 'No session or seats');
        }

        $in_purchased_seats = explode(',', $seats);
        $purchased_seats = [];

        $session = Yii::app()->db->createCommand()
                                 ->select('s.idsession as session_id')
                                 ->from('api.session as s')
                                 ->where('s.idsession = :idsession AND s.sessionts > now()', [':idsession' => $idsession])
                                 ->queryScalar();
        if (is_null($session))
        {
            throw new CHttpException(404, 'No such session');
        }

        $maxseats = Yii::app()->db->createCommand()
                                  ->select('h.seats as maxseats')
                                  ->from('api.session as s')
                                  ->join('api.hall as h', 's.idhall = h.idhall')
                                  ->where('s.idsession = :idsession', [':idsession' => $idsession])
                                  ->queryScalar();

        foreach ($in_purchased_seats as $purchased_seat)
        {
            if (is_numeric($purchased_seat))
            {
                if ($purchased_seat > $maxseats or 0 >= $purchased_seat)
                {
                    throw new CHttpException(400, 'Wrong seat');
                }
                $purchased_seats[] = $purchased_seat;
            }
        }

        $occupied_seats = Yii::app()->db->createCommand()
                                        ->select('t.seat as occupied_seat')
                                        ->from('api.ticket as t')
                                        ->join('api.order as o', 'o.idorder = t.idorder and o.isactive is true')
                                        ->join('api.session as s', 's.idsession = o.idsession and s.idsession = :idsession', [':idsession' => $idsession])
                                        ->where(['in', 't.seat', $purchased_seats])
                                        ->queryAll();

        if (is_array($occupied_seats) and 0 < sizeof($occupied_seats))
        {
            throw new CHttpException(409, 'Some seats are occupied');
        }

        $buy_transaction = Yii::app()->db->beginTransaction();

        $order = new Order();

        $idorder = md5(uniqid());

        $order->attributes = [
            'idorder'   => $idorder,
            'isactive'  => true,
            'idsession' => $idsession
        ];

        if (!$order->save())
        {
            throw new CHttpException(500, 'Order creation error');
        }

        foreach ($purchased_seats as $purchased_seat)
        {
            $ticket = new Ticket();

            $ticket->attributes = [
                'idorder' => $idorder,
                'seat'    => $purchased_seat
            ];

            if (!$ticket->save())
            {
                throw new CHttpException(500, 'Ticket creation error');
            }
        }

        $buy_transaction->commit();

        $this->sendResponse(200, CJSON::encode(['idorder' => $idorder, 'isactive' => true]));
    }

    public function actionReject($idorder)
    {
        if (null === ($order = Order::model()->findByPk($idorder)))
        {
            throw new CHttpException(404, 'No such order');
        }

        $session = Session::model()->findByPk($order->idsession);

        $sessiondt = new DateTime($session->sessionts);
        $nowdt     = new DateTime('now');
        $nowdt->add(new DateInterval('PT1H'));

        if ($sessiondt < $nowdt)
        {
            throw new CHttpException(410, 'Too late to reject');
        }

        if (!$order->isactive)
        {
            throw new CHttpException(409, 'Order already rejected');
        }

        $order->isactive = false;

        if (!$order->save())
        {
            throw new CHttpException(500, 'Order reject error');
        }

        $this->sendResponse(200, CJSON::encode(['idorder' => $idorder, 'isactive' => false]));
    }
}
