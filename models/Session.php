<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Session model
 *
 * @category  Model
 * @package   MIF
 * @author    Anton Redozubov <anton@redozubov.ru>
 * @copyright 2014 Anton Redozubov
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://redozubov.ru
 */

/**
 * This is the model class for table "api.session".
 *
 * The followings are the available columns in table 'api.session':
 *
 * @property integer $idsession
 * @property string  $idfilm
 * @property string  $idhall
 * @property string  $sessionts
 */
class Session extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return Session the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'api.session';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['idfilm, idhall, sessionts', 'required'],
            ['idfilm, idhall', 'length', 'max' => 32],
            ['idsession, idfilm, idhall, sessionts', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'order' => [self::HAS_MANY, 'Order', 'idorder'],
            'hall'  => [self::BELONGS_TO, 'Hall', 'idhall'],
            'film'  => [self::BELONGS_TO, 'Film', 'idfilm'],
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'idsession' => 'Idsession',
            'idfilm'    => 'Idfilm',
            'idhall'    => 'Idhall',
            'sessionts' => 'Sessionts',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idsession', $this->idsession);
        $criteria->compare('idfilm', $this->idfilm, true);
        $criteria->compare('idhall', $this->idhall, true);
        $criteria->compare('sessionts', $this->sessionts, true);

        return new CActiveDataProvider($this, ['criteria' => $criteria,]);
    }
}