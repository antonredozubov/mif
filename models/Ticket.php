<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Ticket model
 *
 * @category  Model
 * @package   MIF
 * @author    Anton Redozubov <anton@redozubov.ru>
 * @copyright 2014 Anton Redozubov
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://redozubov.ru
 */

/**
 * This is the model class for table "api.ticket".
 *
 * The followings are the available columns in table 'api.ticket':
 *
 * @property integer $idticket
 * @property string  $idorder
 * @property integer $seat
 */
class Ticket extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className active record class name.
     *
     * @return Ticket the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'api.ticket';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return [
            ['idorder, seat', 'required'],
            ['seat', 'numerical', 'integerOnly' => true],
            ['idorder', 'length', 'max' => 32],
            ['idticket, idorder, seat', 'safe', 'on' => 'search'],
        ];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'order' => [self::BELONGS_TO, 'Order', 'idorder'],];
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'idticket' => 'Idticket',
            'idorder'  => 'Idorder',
            'seat'     => 'Seat',
        ];
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idticket', $this->idticket);
        $criteria->compare('idorder', $this->idorder, true);
        $criteria->compare('seat', $this->seat);

        return new CActiveDataProvider($this, ['criteria' => $criteria,]);
    }
}