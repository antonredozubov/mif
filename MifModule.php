<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * MIF Module
 * Test work
 *
 * @category  Module
 * @package   MIF
 * @author    Anton Redozubov <anton@redozubov.ru>
 * @copyright 2014 Anton Redozubov
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://redozubov.ru
 */

class MifModule extends CWebModule
{
    public function init()
    {
        parent::init();

        $this->setImport(
             [
             'mif.models.*',
             'mif.components.*',
             ]);
    }

    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
