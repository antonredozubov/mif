<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * MD5 helper
 *
 * @category  Component
 * @package   MIF
 * @author    Anton Redozubov <anton@redozubov.ru>
 * @copyright 2014 Anton Redozubov
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://redozubov.ru
 */

class MD5
{
    public static function isValidMd5($md5 = '')
    {
        return strlen($md5) == 32 && ctype_xdigit($md5);
    }
}