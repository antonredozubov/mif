<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * JSON wrapper
 *
 * @category  Component
 * @package   MIF
 * @author    Anton Redozubov <anton@redozubov.ru>
 * @copyright 2014 Anton Redozubov
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://redozubov.ru
 */

class JSON extends CJSON
{
    public static function encode($var)
    {
        if ('object' === gettype($var) && method_exists($var, 'toJSON'))
        {
            $var = $var->toJSON();
        }

        return parent::encode($var);
    }

    protected static function nameValue($name, $value)
    {
        return static::encode(strval($name)) . ':' . static::encode($value);
    }
}
