<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Controller is the customized base controller class.
 * All controller classes for this module should extend from this base class.
 *
 * @category  Component
 * @package   MIF
 * @author    Anton Redozubov <anton@redozubov.ru>
 * @copyright 2014 Anton Redozubov
 * @license   http://www.php.net/license/3_0.txt  PHP License 3.0
 * @link      http://redozubov.ru
 */

class Controller extends CController
{
    /**
     * Install exception handlers.
     */
    public function init()
    {
        parent::init();

        // Install uncaught exception handler
        Yii::app()->attachEventHandler('onException', array($this, 'onException'));
    }

    /**
     * Handle uncaught exception.
     *
     * @param CExceptionEvent $event
     */
    public function onException($event)
    {
        $this->sendResponse($event->exception->statusCode, JSON::encode(['status' => $event->exception->statusCode, 'message' => $event->exception->getMessage()]));
    }

    /**
     * Define access control, actions, for entire module
     */
    public function accessRules()
    {
        return [['allow', 'users' => ['@']], ['deny', 'users' => ['*']],];
    }

    /**
     * Gets RestFul data and decodes its JSON request
     *
     * @return mixed
     */
    public function getJsonInput()
    {
        return CJSON::decode(file_get_contents('php://input'));
    }

    /**
     * Send raw HTTP response
     *
     * @param int    $status      HTTP status code
     * @param string $body        The body of the HTTP response
     * @param string $contentType Header content-type
     *
     * @return HTTP response
     */
    protected function sendResponse($status = 200, $body = '', $contentType = 'application/json')
    {
        // Set the status
        $statusHeader = 'HTTP/1.1 ' . $status . ' ' . $this->getStatusCodeMessage($status);
        header($statusHeader);
        // Set the content type
        header('Content-type: ' . $contentType);

        echo $body;
        Yii::app()->end();
    }

    /**
     * Return the http status message based on integer status code
     *
     * @param int $status HTTP status code
     *
     * @return string status message
     */
    protected function getStatusCodeMessage($status)
    {
        $codes = [
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported',
        ];

        return (isset($codes[$status])) ? $codes[$status] : '';
    }
}
